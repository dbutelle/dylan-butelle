<html>
    <head>
        <?php include "addons/header.html";?>
        <title>A propos | Dylan Butelle</title>
    </head>
    <body>

    <article>
        <div id="about-intro">
            <div class="container">
                <div class="row">
                    <div id="about-intro-text" class="col-lg-7">
                        <h1>A propos de moi</h1>
                        <p>
                            Passionné par l'informatique depuis que je suis enfant, j’aime toujours apprendre de nouvelles choses chaque jour.
                            Avec un Bac S et quelques projets web qui me suivent depuis plusieurs années,
                            je sais comment résoudre des problèmes liés à mon domaine favori qu'est le développement web.
                        </p>
                        <p>
                            En tant que fondateur d’une association de maintenance informatique,
                            j’essaie de me tenir informé des dernières nouveautés du marché
                            des nouvelles technologies afin de répondre au mieux à nos nombreux clients
                            (hardware, software, nouvelles technologies, etc. )
                        </p>
                        <p>
                            Fan de nouveautés, j’aime apprendre de nouvelles choses et les enseigner aux autres.
                            Créer de nouveaux projets en équipe, ou seul, tout créer de zéro
                            et aimer ce que nous faisons parce que c’est plus qu’une simple passion, c’est un mode de vie
                            que j’ai choisi de suivre.
                        </p>
                    </div>
                    <div id="about-intro-image" class="col-lg-5">
                    </div>
                </div>
            </div>
        </div>

        <div id="about-content">
            <div class="container">
                <!-- Personal info -->
                <h2>Informations personnelles</h2>
                <table class="table">
                    <tr>
                        <th style="border-top: none">Âge</th>
                        <td style="border-top: none">20 ans</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>
                            <a href="mailto:butelledylan@gmail.com">
                                butelledylan@gmail.com
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <th>Plus</th>
                        <td><a href="./downloads/CV2021.pdf">CV</a></td>
                    </tr>
                </table>

                <h2>Ma vie</h2>

                <div class="card">
                    <h3>Création d'une boutique e-commerce</h3>
                    <p>
                        <span class="badge badge-pill">Juillet 2019 → Janvier 2021</span>
                        <span class="badge badge-pill">Watch'N'Wear</span>
                        <span class="badge badge-pill">France</span>
                    </p>
                    <table class="table">
                        <tr>
                            <th>Intitulé</th>
                            <td>
                                Création d'une boutique de e-commerce. (Auto-entreprise)
                            </td>
                        </tr>
                        <th>Plus</th>
                        <td>
                            Développement du site, gestion SAV, gestion commande et gestion des réseaux sociaux
                        </td>
                    </table>
                </div>

                <div class="card">
                    <h3>Obtention du BAFA</h3>
                    <p>
                        <span class="badge badge-pill">October 2017 → Novembre 2018</span>
                        <span class="badge badge-pill">Boulogne-sur-mer</span>
                    </p>
                    <table class="table">
                        <tr>
                            <th>Intitulé</th>
                            <td>
                                Obtention du brevet d'aptitude aux fonctions d'animateur
                            </td>
                        </tr>
                        <tr>
                            <th>Plus</th>
                            <td>
                                Stage rémunéré en ALSH de Juillet 2018 à Août 2018.
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="card">
                    <h3>Création d'une association à but non lucratif</h3>
                    <p>
                        <span class="badge badge-pill">Janvier 2017 → Aujourd'hui</span>
                        <span class="badge badge-pill">Q&D Informatique</span>
                        <span class="badge badge-pill">France</span>
                    </p>
                    <table class="table">
                        <tr>
                            <th>Intitulé</th>
                            <td>
                                Président d'une association de maintenance informatique.
                            </td>
                        </tr>
                        <tr>
                            <th>Plus</th>
                            <td>
                                Développement du site web (Wordpress) et des facturations clients.
                            </td>
                        </tr>
                    </table>
                </div>

                <h2>Education</h2>

                <div class="card">
                    <h3>Diplôme du baccalauréat général série S</h3>
                    <p>
                        <span class="badge badge-pill">2018</span>
                        <span class="badge badge-pill">Lycée Auguste Mariette</span>
                        <span class="badge badge-pill">Boulogne-sur-mer</span>
                    </p>
                    <table class="table">
                        <tr>
                            <th>Plus</th>
                            <td>
                                Spécialité mathématiques.
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="card">
                    <h3>Diplôme national du brevet</h3>
                    <p>
                        <span class="badge badge-pill">2015</span>
                        <span class="badge badge-pill">Collège Jean-Moulin</span>
                        <span class="badge badge-pill">Le Portel</span>
                    </p>
                    <table class="table">
                        <tr>
                            <th>Plus</th>
                            <td>
                                Mention Bien.
                            </td>
                        </tr>
                    </table>
                </div>

            </div>
        </div>


    </article>
    </body>
   <footer>
       <?php include "addons/footer.html";?>
    </footer>

</html>