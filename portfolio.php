<html>
    <head>
        <?php include "addons/header.html";?>
        <title>Portfolio | Dylan Butelle</title>
    </head>
    <body>
    <article>
        <div id="portfolio-intro">
            <div class="container">
                <h1>Portfolio</h1>

                <h2>Projets personnels et créations</h2>

                <p>
                    Vous trouverez sur cette page une collection de projet que j'ai réalisé durant mon temps libre ou durant
                    mes années à l'université. Chaque projet est soit en ligne (consultable par n'importe qui), soit
                    sous gitlab avec des screens sous la présentation de chaque projet.
                </p>
            </div>
        </div>

        <div id="portfolio-content">
            <div class="container">
                <h2 id="devweb">Développement web</h2>

                <div class="card-columns">
                    <div class="card">

                        <h3> 🔵  Jarvis Interface</h3>

                        <hr>
                        <h4>Description</h4>
                        <p>          <em>Jarvis Interface</em>
                        est un système de connexion basique en PHP utilisé par
                            Tony Stark pour accéder à Jarvis. Le code présente également une
                            partie Javascript ainsi qu'une couche de css pour rendre
                            le charme à la Iron Man.
                        </p>


                        <hr>

                        <p>
                            <img src="./downloads/Jarvis1.PNG" alt="🔵  Jarvis Connexion" class="img-fluid" />
                            <img src="./downloads/Jarvis2.PNG" alt="🔵  Jarvis Connexion" class="img-fluid" />
                        </p>

                        <hr>
                        <h4>Plus</h4>
                        <p><a href="http://jarvis.qdinformatique.com/">Jarvis Interface</a></p>
                        <p> <a href="https://gitlab.com/florianlprt/ai.io">Dépôt Gitlab</a></p>
                        <p><em>Username : stark ; Password : warmachinerox</em></p>

                    </div>


                    <div class="card">

                        <h3>🎲 Secret Word Game</h3>

                        <hr>
                        <h4>Description</h4>
                        <p>          <em>Secret Word Game</em> est un jeu web développé en PHP où le but est de retrouvé
                        le mot caché derrière.
                        Le nombre d'essai est infini mais l'envie de découvrir le mot est de plus en plus grande.
                            Saurez-vous retrouver le mot caché ?
                        </p>


                        <hr>
                        <p>
                            <img src="./downloads/swg.PNG" alt="🎲 Secret Word Game" class="img-fluid" />
                        </p>


                        <hr>
                        <h4>Plus</h4>
                        <p><a href="http://swg.qdinformatique.com/">Secret Word Game</a></p>
                        <p><a href="https://gitlab.com/florianlprt/sialac_optibus">Dépôt Gitlab</a></p>


                    </div>


                    <div class="card">

                        <h3>🔮 Magic Store</h3>

                        <hr>
                        <h4>Description</h4>
                        <p>          <em>Magic Store</em>
                        est une boutique en ligne de carte Magic développé en PHP. Les cartes et le panier sont
                            générées en Javascript via un fichier JSON.
                        </p>
                        <hr>
                        <p>
                            <img src="./downloads/magicstore1.PNG" alt="🔮 Magic Store" class="img-fluid" />
                            <img src="./downloads/magicstore2.PNG" alt="🔮 Magic Store" class="img-fluid" />
                        </p>

                        <hr>
                        <h4>More</h4>
                        <p><a href="https://gitlab.com/dbutelle/magicstore">Dépôt Gitlab</a></p>
                        <p><a href="http://magicstore.qdinformatique.com/">Magic Store</a></p>
                        <p><em>Username : gandalf ; Password : youshallnotpass</em></p>

                    </div>


                    <div class="card">

                        <h3>🚀 SpaceView</h3>

                        <hr>
                        <h4>Description</h4>
                        <p>          <em>Spaceview</em>
                            est une boutique en ligne de vente de téléscope. Ce site a été créé dans le but d'un projet de fin
                            de semestre de deuxième année de licence. La boutique a été créée à base de PHP, HTML, CSS et Javascript
                            en respectant le modèle MVC (Modèle-Vue-Contrôleur).
                        </p>

                        <hr>
                        <p>
                            <img src="./downloads/spaceview1.PNG" alt="🚀 SpaceView" class="img-fluid" />
                            <img src="./downloads/spaceview2.PNG" alt="🚀 SpaceView" class="img-fluid" />
                        </p>


                        <hr>
                        <h4>Plus</h4>
                        <p><a href="http://spaceview.qdinformatique.com/">Space View</a></p>

                        <p><a href="https://gitlab.com/dbutelle/ulco-l2-projetweb-etudiant">Dépôt Gitlab</a></p>

                    </div>

                </div>

                <h2 id="java">Java</h2>

                <div class="card-columns">

                    <div class="card">
                        <h3>⚔️ Learning Souls Game</h3>

                        <hr>
                        <h4>Description</h4>
                        <p>          <em>Learning Souls Game</em>
                        est un projet de jeu vidéo en tour par tour type RPG développé en Java. Le but est de
                            récupérer des armures et armes sur les zombies entrant tout en survivant aux attaques.
                            Attention à votre Stamina et à vos points de vie.
                        </p>
                        <hr>
                        <p>
                            <img src="./downloads/lsg.JPG" alt="⚔️ Learning Souls Game" class="img-fluid" />
                        </p>
                    </div>


                    <div class="card">
                        <h3>🃏 Poker et bataille en réseau</h3>
                        <hr>
                        <h4>Description</h4>
                        <p>          <em>Poker et bataille en réseau</em>
                            est un projet Java réalisé dans le cadre la L2 Informatique. Le but principal
                            était de créer un jeu de bataille et de poker (légérement simplifié), le tout en réseaux.
                        </p>
                        <hr>
                        <p>
                            <img src="./downloads/bataille.PNG" alt="🃏 Poker et bataille en réseau" class="img-fluid" />
                        </p>
                        <h4>Plus</h4>

                        <p><a href="https://gitlab.com/dbutelle/l2_projet_java">Dépôt Gitlab</a></p>

                    </div>



                </div>


                <h2 id="cpp">C++</h2>

                <div class="card-columns">

                    <div class="card">
                        <h3>🔳 Labyrinthe</h3>
                        <hr>
                        <h4>Description</h4>
                        <p>          <em>Labyrinthe</em>
                        est un générateur et résolveur automatique de labyrinthe. Il s'agît d'un programme
                            en C++ où en entrant simplement la taille du labyrinthe, il se construit, donne
                            tous les chemins possibles du labyrinthe et donne le chemin le plus court.
                        </p>

                        <hr>
                        <p>
                            <img src="./downloads/labyrinthe.PNG" alt="🔳 Labyrinthe" class="img-fluid" />
                        </p>
                    </div>
                </div>

    </article>
    </body>
    <footer>
        <?php include "addons/footer.html";?>
    </footer>
</html>